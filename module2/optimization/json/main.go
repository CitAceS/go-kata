// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    pets, err := UnmarshalPets(bytes)
//    bytes, err = pets.Marshal()

package main

import (
	"encoding/json"
	"fmt"

	jsoniter "github.com/json-iterator/go"
)

var jsonData = []byte(`
	 {
    "id": 32290437,
    "category": {
      "id": -55683116,
      "name": "in commodo nostrud voluptate irure"
    },
    "name": "doggie",
    "photoUrls": [
      "amet",
      "consequat in"
    ],
    "tags": [
      {
        "id": 12710057,
        "name": "dolore eu"
      },
      {
        "id": -39970071,
        "name": "et sint"
      }
    ],
    "status": "pending"
  }
`)

type Pets Pet

func UnmarshalPets(data []byte) (Pets, error) {
	var r Pets
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Pets) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type Pet struct {
	ID        float64    `json:"id"`
	Category  Category   `json:"category"`
	Name      string     `json:"name"`
	PhotoUrls []string   `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    string     `json:"status"`
}

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

func (r *Pets) Marshal2() ([]byte, error) {
	return json.Marshal(r)
}
func UnmarshalPets2(data []byte) (Pets, error) {
	var r Pets
	err := jsoniter.Unmarshal(data, &r)
	return r, err
}

func main() {
	fmt.Println(json.Valid(jsonData))
}
