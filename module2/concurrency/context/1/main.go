package main

import (
	"context"
	"fmt"
	"time"
)

type key int

const (
	userIDctx key = 0
)

func main() {
	ctx := context.WithValue(context.Background(), userIDctx, 1)
	ctx, cancel := context.WithCancel(ctx)

	go func() {

		fmt.Scanln()
		cancel()

	}()

	processLongTask(ctx)
}

func processLongTask(ctx context.Context) {
	id := ctx.Value(userIDctx)

	select {
	case <-time.After(2 * time.Second):
		fmt.Println("done processing id", id)
	case <-ctx.Done():
		fmt.Println("request canceled")
	}
}
