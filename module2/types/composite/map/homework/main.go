// You can edit this code!
// Click here and start typing.
package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		// сюда впишите ваши остальные 12 структур
		{
			Name:  "https://github.com/containerd/project",
			Stars: 60,
		},
		{
			Name:  "https://github.com/getActivity/AndroidProject",
			Stars: 5500,
		},
		{
			Name:  "https://github.com/finos/open-developer-platform",
			Stars: 41,
		},
		{
			Name:  "https://github.com/antaresproject/project",
			Stars: 87,
		},
		{
			Name:  "https://github.com/521xueweihan/HelloGitHub",
			Stars: 65000,
		},
		{
			Name:  "https://github.com/mbeaudru/modern-js-cheatsheet",
			Stars: 24300,
		},
		{
			Name:  "https://github.com/ChristosChristofidis/awesome-deep-learning",
			Stars: 20300,
		},
		{
			Name:  "https://github.com/bradtraversy/vanillawebprojects",
			Stars: 14300,
		},
		{
			Name:  "https://github.com/ansible/awx",
			Stars: 11900,
		},
		{
			Name:  "https://github.com/mkdocs/mkdocs",
			Stars: 15900,
		},
		{
			Name:  "https://github.com/ipader/SwiftGuide",
			Stars: 15800,
		},
		{
			Name:  "https://github.com/ipader/awesome-iot",
			Stars: 266,
		},
	}
	m := make(map[string]Project)
	// в цикле запишите в map
	for _, elem := range projects {
		m[elem.Name] = elem
	}
	// в цикле пройдитесь по мапе и выведите значения в консоль
	for _, elem := range m {
		fmt.Println(elem)
	}
}
