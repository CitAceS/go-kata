// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	fmt.Println(n == nil)
	test(n)
}

func test(r interface{}) {
	checkNil := fmt.Sprintf("%v", r)
	switch r.(type) {
	case interface{}:
		if checkNil == "<nil>" {
			fmt.Println("Success!")
		}
	}
}
