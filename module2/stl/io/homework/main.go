// You can edit this code!
// Click here and start typing.
package main

import (
	"bytes"
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	// здесь расположите буфер
	var buff, newBuff bytes.Buffer
	// запишите данные в буфер
	for _, elem := range data {
		buff.WriteString(elem + "\n")
	}
	// создайте файл
	o, err := os.Create("module2/stl/io/homework/example.txt")
	check(err)
	defer o.Close()
	// запишите данные в файл
	_, err = o.Write(buff.Bytes())
	check(err)
	// прочтите данные в новый буфер
	r, err := os.ReadFile("module2/stl/io/homework/example.txt")
	check(err)
	newBuff.Write(r)
	fmt.Println(newBuff.String()) // выведите данные из буфера buffer.String()
	// у вас все получится!
}
