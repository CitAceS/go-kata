package main

import (
	"encoding/json"
	"fmt"
)

type Student struct {
	FirstName, lastName string
	Email               string
	Age                 int
	HeightInMeters      float64
	IsMale              bool
}

type Students map[string]interface{}

type Profile struct {
	Username  string
	followers int
	Grades    map[string]string
}

type Children struct {
	Firstname, lastname string
	Age                 int
	Profile             Profile
	Languages           []string
}

func main() {
	john := Student{FirstName: "John", lastName: "Doe", Age: 21, HeightInMeters: 1.75, IsMale: true}
	johnJSON, _ := json.Marshal(john)
	fmt.Println(string(johnJSON))
	//{"FirstName":"John","Email":"","Age":21,"HeightInMeters":1.75,"IsMale":true}
	joh := Students{"FirstName": "John", "lastName": "Doe", "Age": 21, "HeightInMeters": 1.75, "IsMale": true}
	johJSON, _ := json.Marshal(joh)
	fmt.Println(string(johJSON))
	//{"Age":21,"FirstName":"John","HeightInMeters":1.75,"IsMale":true,"lastName":"Doe"}
	jo := Children{
		Firstname: "John",
		lastname:  "Doe",
		Age:       21,
		Profile: Profile{
			Username:  "johndoe91",
			followers: 1975,
			Grades:    map[string]string{"Math": "A", "Science": "A+"},
		},
		Languages: []string{"English", "French"},
	}
	joJSON, _ := json.MarshalIndent(jo, "", " ")
	fmt.Println(string(joJSON))
}
