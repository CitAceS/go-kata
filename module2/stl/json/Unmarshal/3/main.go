package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	Username  string
	Followers int
}
type Student struct {
	FirstName      string
	HeightInMeters float64
	IsMale         bool
	Languages      [2]string
	Subjects       []string
	Grades         map[string]string
	Profile        Profile
}

func main() {
	data := []byte(`
	{
		"FirstName": "John",
		"HeightInMeters": 1.75,
		"IsMale": null,
		"Languages": [ "English", "Spanish", "German" ],
		"Subjects": [ "Math", "Science" ],
		"Grades": { "Math": "A","Math": "S" },
		"Profile": {
			"Username": "johndoe91",
			"Followers": 1975
		}
	}`)
	var john Student = Student{
		IsMale:   true,
		Subjects: []string{"Art"},
		Grades:   map[string]string{"Science": "A+"},
	}
	fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))
	fmt.Printf("%#v\n", john)
}

/*
Error: <nil>
main.Student{FirstName:"John", HeightInMeters:1.75, IsMale:true, Languages:[2]string{"English", "Spanish"}, Subjects:[]string{"Math", "Science"}, Grades:map[string]string{"Math":"A", "Science":"A+"}, Profile:main.Profile{Username:"johndoe91", Followers:1975}}
*/
