package main

import (
	"encoding/json"
	"fmt"
	"strings"
)

type Person struct {
	Name string
	Age  int
}

func main() {
	jsonStream := strings.NewReader(`
{"Name":"Ross Geller","Age":28}
{"Name":"Monica Geller","Age":27}
{"Name":"Jack Geller","Age":56}
`)
	decoder := json.NewDecoder(jsonStream)
	var ross, monica Person
	err := decoder.Decode(&ross)
	if err != nil {
		panic(err)
	}
	err = decoder.Decode(&monica)
	if err != nil {
		panic(err)
	}
	fmt.Printf("ross: %#v\n", ross)
	fmt.Printf("monica: %#v\n", monica)
}

/*
ross: main.Person{Name:"Ross Geller", Age:28}
monica: main.Person{Name:"Monica Geller", Age:27}
*/
