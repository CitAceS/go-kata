package main

import (
	"fmt"
)

type Person struct {
	Name  string
	Age   int
	Money float64
}

func generateSelfStory(name string, age int, money float64) string {
	return fmt.Sprintf("\"Hello! My name is %s. I'm %d y.o. And I also have $%.2f in my wallet right now.\"", name, age, money)
}

func main() {
	p := Person{Name: "Andy", Age: 18, Money: 10}
	fmt.Println(generateSelfStory(p.Name, p.Age, p.Money))
}
