package main

import (
	"fmt"
	"unicode"
)

func Greet(name string) string {
	if checkRusName(name) {
		return fmt.Sprintf("Привет %s, добро пожаловать!", name)
	}
	return fmt.Sprintf("Hello %s, you welcome!", name)
}

func checkRusName(s string) bool {
	var isTrue bool
	for _, elem := range s {
		if !unicode.Is(unicode.Latin, elem) {
			isTrue = true
		}
	}
	return isTrue
}

// to PASS stage 3 compile on gitlab.com did a few examples:
func main() {
	fmt.Println(Greet("Alexey"))
	fmt.Println(Greet("Юрий"))
}
