package main

import "testing"

var Users = genUsers()
var Products = genProducts()

func BenchmarkMapUserProducts(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts(Users, Products)
	}
}
func BenchmarkMapUserProducts2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts2(Users, Products)
	}
}
