package repository

import (
	"context"

	"gitlab.com/citaces/go-kata/module5/review/models"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type Repository interface {
	Create(context.Context, models.User) error
	Delete(context.Context, int) error
	Update(context.Context, models.User) error
	List(context.Context) ([]models.User, error)
}

type PostgresRepository struct {
	db *sqlx.DB
}

func NewPostgresRepository(db *sqlx.DB) *PostgresRepository {
	return &PostgresRepository{
		db: db,
	}
}

func (p PostgresRepository) Create(ctx context.Context, user models.User) error {
	_, err := p.db.ExecContext(ctx, `INSERT INTO cities (name,state) VALUES ($1,$2)`, user.Name, user.State)
	if err != nil {
		return err
	}
	return nil
}

func (p PostgresRepository) Delete(ctx context.Context, id int) error {
	_, err := p.db.ExecContext(ctx, `DELETE FROM cities WHERE id =$1`, id)
	if err != nil {
		return err
	}
	return nil
}

func (p PostgresRepository) Update(ctx context.Context, user models.User) error {
	_, err := p.db.ExecContext(ctx, `UPDATE cities SET name=$1, state=$2 WHERE id=$3`, user.Name, user.State, user.Id)
	if err != nil {
		return err
	}
	return nil
}

func (p PostgresRepository) List(ctx context.Context) ([]models.User, error) {
	var table []models.User
	err := p.db.SelectContext(ctx, &table, `SELECT * FROM cities`)
	if err != nil {
		return []models.User{}, err
	}
	return table, nil
}
