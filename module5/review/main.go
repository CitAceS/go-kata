package main

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/citaces/go-kata/module5/review/models"
	"gitlab.com/citaces/go-kata/module5/review/repository"

	"github.com/jmoiron/sqlx"
)

func main() {
	/*
		postgres, err := sqlx.Open(`postgres`, "host=localhost port=5432 user=postgres password=postgres dbname=postgres sslmode=disable")
		if err != nil {
			log.Fatal(err)
		}
	*/
	postgres, err := sqlx.Open(`postgres`, "host=94.103.95.135 port=5432 user=postgres password=1c3324d4-9ec0-4623-8793-e55ead1ad8ae sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	err = postgres.Ping()
	if err != nil {
		log.Fatal(err)
	}
	_, err = postgres.Exec(`CREATE TABLE IF NOT EXISTS cities(
    id SERIAL PRIMARY KEY,
    name VARCHAR(30) NOT NULL,
    state VARCHAR(30) NOT NULL
)`)
	if err != nil {
		log.Fatal(err)
	}

	repo := repository.NewPostgresRepository(postgres)

	user1 := models.User{
		Name:  `Vasya`,
		State: `Aidaho`,
	}
	user2 := models.User{
		Name:  `Ivan`,
		State: `Alabama`,
	}
	user3 := models.User{
		Name:  `John`,
		State: `Aiova`,
	}

	err = repo.Create(context.Background(), user1)
	if err != nil {
		log.Fatal(err)
	}
	err = repo.Create(context.Background(), user2)
	if err != nil {
		log.Fatal(err)
	}
	err = repo.Create(context.Background(), user3)
	if err != nil {
		log.Fatal(err)
	}
	err = repo.Update(context.Background(), models.User{Id: 1, Name: `Boris`, State: `Alaska`})
	if err != nil {
		log.Fatal(err)
	}
	err = repo.Delete(context.Background(), 2)
	if err != nil {
		log.Fatal(err)
	}
	users, err := repo.List(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(users)
}
