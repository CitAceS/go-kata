package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"testing"
	"time"

	"github.com/brianvoe/gofakeit/v6"
)

const (
	DateTime = "2006-01-02 15:04:05"
)

func Show(dll *DoubleLinkedList, method string) {
	curr := dll.head
	for curr != nil {
		fmt.Printf("-> %s -> %v\n", method, curr.data)
		curr = curr.next
	}
	fmt.Println()
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func NewDoubleLinkedList() *DoubleLinkedList {
	return &DoubleLinkedList{}
}

func newNode() *Node {
	return &Node{
		data: &Commit{
			Message: gofakeit.Street(),
			UUID:    gofakeit.UUID(),
			Date:    gofakeit.Date(),
		},
	}
}

func BenchmarkDoubleLinkedList_QuickSort(b *testing.B) {
	var c []Commit
	o, err := os.Open("./generate.json")
	dec := json.NewDecoder(o)
	if err != nil {
		log.Fatal(err)
	}
	err = dec.Decode(&c)
	if err != nil {
		log.Fatal(err)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		QuickSort(c)
	}
}

func TestDoubleLinkedList_Len(t *testing.T) {
	dll := NewDoubleLinkedList()
	node := newNode()
	err := dll.Insert(0, *node.data)
	check(err)
	err = dll.Insert(0, *node.data)
	check(err)
	err = dll.Insert(0, *node.data)
	check(err)
	if err != nil {
		fmt.Println(err)
	}
	expectingLen(t, dll, 3)
}

func TestQuickSort(t *testing.T) {
	testTime, _ := time.Parse(DateTime, "2006-01-02 15:04:05")
	testCommit := &Commit{Message: "test msg", UUID: "test-test-uuid", Date: testTime}
	testTime2, _ := time.Parse(DateTime, "2005-01-02 15:04:05")
	testCommit2 := &Commit{Message: "test msg", UUID: "test-test-uuid", Date: testTime2}
	testTime3, _ := time.Parse(DateTime, "2004-01-02 15:04:05")
	testCommit3 := &Commit{Message: "test msg", UUID: "test-test-uuid", Date: testTime3}
	testTime4, _ := time.Parse(DateTime, "2003-01-02 15:04:05")
	testCommit4 := &Commit{Message: "test msg", UUID: "test-test-uuid", Date: testTime4}

	arr := []Commit{*testCommit2, *testCommit3, *testCommit, *testCommit4}
	correctArr := []Commit{*testCommit4, *testCommit3, *testCommit2, *testCommit}

	expectingQuickSort(t, arr, correctArr)
}

func TestDoubleLinkedList_SingleNode(t *testing.T) {
	dll := NewDoubleLinkedList()
	node := newNode()
	err := dll.Insert(0, *node.data)
	check(err)
	expectingSingleNode(t, dll, node)
}

func TestDoubleLinkedList_EmptyNode(t *testing.T) {
	dll := NewDoubleLinkedList()
	node := newNode()
	err := dll.Insert(0, *node.data)
	check(err)
	fmt.Println(dll.head.data)
	err = dll.Delete(1)
	check(err)
	expectingEmpty(t, dll)
}

func TestDoubleLinkedList_DeleteLast(t *testing.T) {
	dll := NewDoubleLinkedList()
	node := newNode()
	err := dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	Show(dll, "TestDoubleLinkedList_DeleteLast")
	check(err)
	err = dll.Delete(4)
	Show(dll, "TestDoubleLinkedList_DeleteLast")
	check(err)
	expectingCutNode(t, dll, 3)
}

func TestDoubleLinkedList_DeleteMid(t *testing.T) {
	dll := NewDoubleLinkedList()
	node := newNode()
	err := dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	Show(dll, "TestDoubleLinkedList_Mid")
	check(err)
	err = dll.Delete(2)
	Show(dll, "TestDoubleLinkedList_Mid")
	check(err)
	expectingCutNode(t, dll, 3)
}
func TestDoubleLinkedList_DeleteFirst(t *testing.T) {
	dll := NewDoubleLinkedList()
	node := newNode()
	err := dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	Show(dll, "TestDoubleLinkedList_first")
	check(err)
	err = dll.Delete(1)
	Show(dll, "TestDoubleLinkedList_first")
	check(err)
	expectingCutNode(t, dll, 3)
}

func TestDoubleLinkedList_InsertLast(t *testing.T) {
	dll := NewDoubleLinkedList()
	node := newNode()
	err := dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	Show(dll, "InsertLast")
	check(err)
	n := newNode()
	err = dll.Insert(4, *n.data)
	Show(dll, "InsertLast")
	check(err)
	expectingInsert(t, dll, 5)
}

func TestDoubleLinkedList_InsertFirst(t *testing.T) {
	dll := NewDoubleLinkedList()
	node := newNode()
	err := dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	Show(dll, "InsertFirst")
	check(err)
	n := newNode()
	err = dll.Insert(0, *n.data)
	Show(dll, "InsertFirst")
	check(err)
	expectingInsert(t, dll, 5)
}

func TestDoubleLinkedList_InsertMid(t *testing.T) {
	dll := NewDoubleLinkedList()
	node := newNode()
	err := dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	Show(dll, "InsertMid")
	check(err)
	n := newNode()
	err = dll.Insert(2, *n.data)
	Show(dll, "InsertMid")
	check(err)
	expectingInsert(t, dll, 5)
}
func TestDoubleLinkedList_Index(t *testing.T) {
	dll := NewDoubleLinkedList()
	node := newNode()
	dll.curr = node
	fmt.Println(dll.curr.data)
	err := dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	idx, err := dll.Index()
	fmt.Println(idx, err, dll.curr.data)
	expectingIndex(t, dll, idx)
}

func TestDoubleLinkedList_Pop(t *testing.T) {
	dll := NewDoubleLinkedList()
	startNode := newNode()
	err := dll.Insert(0, *startNode.data)
	check(err)
	node := newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	Show(dll, "Pop")
	PopNode := dll.Pop()
	Show(dll, "Pop")

	expectingPop(t, startNode, PopNode)
}

func TestDoubleLinkedList_Shift(t *testing.T) {
	dll := NewDoubleLinkedList()
	node := newNode()
	err := dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	cutNode := newNode()
	err = dll.Insert(0, *cutNode.data)
	check(err)
	Show(dll, "Shift")
	ShiftNode := dll.Shift()
	Show(dll, "Shift")

	expectingShift(t, ShiftNode, cutNode)
}
func TestDoubleLinkedList_Search(t *testing.T) {
	dll := NewDoubleLinkedList()

	testTime, _ := time.Parse(DateTime, "2006-01-02 15:04:05")
	testCommit := &Commit{Message: "test msg", UUID: "test-test-uuid", Date: testTime}

	node := &Node{data: testCommit}
	err := dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	cutNode := newNode()
	err = dll.Insert(0, *cutNode.data)
	check(err)
	Show(dll, "Search")
	finder := dll.Search("test msg")
	Show(dll, "Search")
	expectingSearch(t, finder)
}
func TestDoubleLinkedList_Reverse(t *testing.T) {
	dll := NewDoubleLinkedList()
	node := newNode()
	err := dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	node = newNode()
	err = dll.Insert(0, *node.data)
	check(err)
	cutNode := newNode()
	err = dll.Insert(0, *cutNode.data)
	check(err)
	Show(dll, "Before reverse")
	newDll := dll.Reverse()
	Show(newDll, "After reverse")
	expectingReverse(t, dll, newDll)
}

func expectingLen(t *testing.T, dll *DoubleLinkedList, count int) {
	if dll.Len() != count {
		t.Fail()
	}
}

func expectingQuickSort(t *testing.T, arr, rightArr []Commit) {
	s := QuickSort(arr)
	for i, commit := range rightArr {
		if commit != s[i] {
			t.Fail()
		}
	}
}

func expectingSingleNode(t *testing.T, dll *DoubleLinkedList, node *Node) {
	if dll.head.data.UUID != node.data.UUID {
		t.Fail()
	}
	if dll.tail.data.UUID != node.data.UUID {
		t.Fail()
	}
}

func expectingEmpty(t *testing.T, dll *DoubleLinkedList) {
	if dll.head != nil {
		t.Fail()
	}
	if dll.tail != nil {
		t.Fail()
	}
}

func expectingCutNode(t *testing.T, dll *DoubleLinkedList, count int) {
	if dll.Len() != count {
		t.Fail()
	}
}
func expectingInsert(t *testing.T, dll *DoubleLinkedList, count int) {
	if dll.Len() != count {
		t.Fail()
	}
}

func expectingIndex(t *testing.T, dll *DoubleLinkedList, idx int) {
	if idx != 3 {
		t.Fail()
	}
}
func expectingPop(t *testing.T, startNode *Node, popNode *Node) {
	if startNode.data.Message != popNode.data.Message {
		t.Fail()
	}
}
func expectingShift(t *testing.T, shiftNode *Node, cutNde *Node) {
	if shiftNode.data.Message != cutNde.data.Message {
		t.Fail()
	}
}

func expectingReverse(t *testing.T, dll *DoubleLinkedList, newDll *DoubleLinkedList) {
	if dll.head.data.Message != newDll.tail.data.Message {
		t.Fail()
	}
}

func expectingSearch(t *testing.T, curr *Node) {
	if curr == nil {
		t.Fail()
	}
}
