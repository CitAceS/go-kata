package main

func MergeSortGPT(arr []int) []int {
	if len(arr) < 2 { /*Если длина списка меньше 2, то он уже отсортирован*/
		return arr
	}

	middle := len(arr) / 2 /*Находим средний индекс списка и рекурсивно вызываем mergeSort для каждой половины*/
	left := MergeSortGPT(arr[:middle])
	right := MergeSortGPT(arr[middle:])

	return mergeGPT(left, right) /*объединяем две отсортированные половины в один упорядоченный список с помощью функции merge*/
}

func mergeGPT(left, right []int) []int { /*принимает два списка и создает новый список, который содержит все элементы из обоих списком в порядке возрастания*/
	result := make([]int, 0, len(left)+len(right))
	i, j := 0, 0 /*используем два указателя i и j для прохода по каждому списку и добавляем элементы в новый список в порядке возрастания*/
	for i < len(left) && j < len(right) {
		if left[i] <= right[j] {
			result = append(result, left[i])
			i++
		} else {
			result = append(result, right[j])
			j++
		}
	}
	result = append(result, left[i:]...)
	result = append(result, right[j:]...)
	return result
}

func MergeSort(items []int) []int {
	if len(items) < 2 {
		return items
	}
	first := MergeSort(items[:len(items)/2])
	second := MergeSort(items[len(items)/2:])
	return merge(first, second)
}
func merge(a []int, b []int) []int {
	final := []int{}
	i := 0
	j := 0
	for i < len(a) && j < len(b) {
		if a[i] < b[j] {
			final = append(final, a[i])
			i++
		} else {
			final = append(final, b[j])
			j++
		}
	}
	for ; i < len(a); i++ {
		final = append(final, a[i])
	}
	for ; j < len(b); j++ {
		final = append(final, b[j])
	}
	return final
}
