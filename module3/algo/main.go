package main

import "fmt"

func main() {
	data := []int{55, 34, 21, 13, 8, 5, 3, 2, 1, 1}
	fmt.Println(BubbleSort(data))
	fmt.Println(QuickSort(data))
	fmt.Println(MergeSortGPT(data))
}
