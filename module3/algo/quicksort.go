package main

import (
	"math/rand"
	"time"
)

func QuickSort(arr []int) []int {
	if len(arr) <= 1 {
		return arr
	}
	median := arr[rand.Intn(len(arr))]      /*находим медиану, выбирая случайное число*/
	low_part := make([]int, 0, len(arr))    /*cюда записываем числа меньше числа в медиане (pivot)*/
	high_part := make([]int, 0, len(arr))   /*cюда записываем числа больше числа в медиане (pivot)*/
	middle_part := make([]int, 0, len(arr)) /*cюда записываем числа равные медиане (pivot)*/
	for _, item := range arr {
		switch {
		case item < median:
			low_part = append(low_part, item) /*запись чисел меньше медианы*/
		case item == median:
			middle_part = append(middle_part, item) /*запись чисел равные медианы*/
		case item > median:
			high_part = append(high_part, item) /*запись чисел больше медианы*/
		}
	}
	low_part = QuickSort(low_part)
	high_part = QuickSort(high_part)

	low_part = append(low_part, middle_part...) /*распаковываем среднюю часть после всех чисел*/
	low_part = append(low_part, high_part...)   /*далее распаковываем оставшуюся часть*/

	return low_part
}

func randomData(n, max int) func() []int {
	var data []int
	return func() []int {
		if data != nil {
			return data
		}
		rand.Seed(time.Now().UnixNano())
		for i := 0; i < n; i++ {
			data = append(data, rand.Intn(max))
		}
		return data
	}
}

func generateData(n, count, maxValue int) [][]int {
	var data [][]int
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < n; i++ {
		var cycleData []int
		for j := 0; j < count; j++ {
			cycleData = append(cycleData, rand.Intn(maxValue))
		}
		data = append(data, cycleData)
	}
	return data
}
