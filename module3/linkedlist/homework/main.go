package main

import (
	"errors"
	"fmt"
	"time"
)

type Post struct {
	body        string
	publishDate int64
	next        *Post
}

type Feed struct {
	length int
	start  *Post
	end    *Post
}

func (f *Feed) Append(newPost *Post) {
	if f.length == 0 {
		f.start, f.end = newPost, newPost /*Если Feed пустой, он устанавливает новый Post как начало и конец элемента Feed.*/
	} else {
		lp := f.end
		lp.next = newPost
		f.end = newPost /*В противном случае он устанавливает новый Post как элемент end и прикрепляет его к предыдущему Post в списке.*/
	}
	f.length++
	/*Вдобавок к тому, насколько он прост, этот алгоритм теперь имеет сложность большого O(1), также известную как «постоянное время».
	Это означает, что Append будет работать одинаково — независимо от длины конструкции Feed.*/
}

func (f *Feed) Remove(publishDate int64) { /*примет publishDate из Post в качестве аргумента, с помощью которого она определит, какой Post нужно удалить (или связь с каким Post нужно отменить).*/
	if f.length == 0 {
		panic(errors.New("Feed is empty"))
	}
	var previousPost *Post
	currentPost := f.start

	for currentPost.publishDate != publishDate { /*Есть один случай, в котором нам нужно убедиться, что мы покрываем нашу функцию Remove: что если у Feed нет Post с указанным publishDate?	Для простоты функция проверяет отсутствие элемента next Post в Feed перед переходом к нему.	Если next это nil, то вызовется panic, сообщающая нам, что она не может найти Post с таким publishDate.*/
		if currentPost.next == nil {
			panic(errors.New("No such Post found."))
		}

		previousPost = currentPost
		currentPost = currentPost.next /*Если она обнаружит, что элемент start Feed должен быть удален, то просто переназначит его start Feed второму элементу Post в Feed.	В противном случае она перескакивает через каждый из Post’ов в Feed, пока не наткнется на Post, который соответствует тому publishDate, который был передан в качестве аргумента функции.*/
	}
	previousPost.next = currentPost.next /*Когда функция найдет и удалит нужный Post, она просто соединит предыдущий и следующий Post’ы.*/

	f.length--

}

func (f *Feed) Insert(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
	} else {
		var previousPost *Post
		currentPost := f.start

		for currentPost.publishDate < newPost.publishDate {
			previousPost = currentPost
			currentPost = previousPost.next
		}

		previousPost.next = newPost
		newPost.next = currentPost
	}
	f.length++
	/*если Post’ы необходимо расположить в правильном порядке в зависимости от publishDate в Feed. По сути, этот алгоритм очень похож на алгоритм в функции Remove, потому что, хотя они делают совершенно разные вещи (добавление или удаление Post в Feed), в их основе лежит поиск. Это означает, что обе функции на самом деле обходят весь массив в Feed в поисках Post’a, совпадающего с publishDate, полученным в аргументе функции.	Единственное отличие заключается в новом значении Insert, который будет помещен Post в то место , где совпадают даты, в то время как Remove удалит Post из Feed. Кроме того, это означает, что обе эти функции имеют одинаковую временную сложность, которая составляет O(n). Это означает, что в худшем случае функции должны будут пройти весь Feed, чтобы добраться до элемента, в который необходимо вставить (или из которого нужно удалить) новый Post*/
}

func (f *Feed) Inspect() {
	if f.length == 0 {
		fmt.Println("Feed is empty")
	}
	fmt.Println("Feed Length: ", f.length)

	currentIndex := 0
	currentPost := f.start

	for currentIndex < f.length {
		fmt.Printf("Item: %v - %v\n", currentIndex, currentPost)
		currentPost = currentPost.next
		currentIndex++
	}
}

func main() {
	rightNow := time.Now().Unix()
	f := &Feed{}
	p1 := &Post{
		body:        "Lorem ipsum",
		publishDate: rightNow,
	}
	p2 := &Post{
		body:        "Dolor sit amet",
		publishDate: rightNow + 10,
	}
	p3 := &Post{
		body:        "consectetur adipiscing elit",
		publishDate: rightNow + 20,
	}
	p4 := &Post{
		body:        "sed do eiusmod tempor incididunt",
		publishDate: rightNow + 30,
	}

	newPost := &Post{
		body:        "This is a new post",
		publishDate: rightNow + 15,
	}

	f.Append(p1)
	f.Append(p2)
	f.Append(newPost)
	f.Append(p3)
	f.Append(p4)

	f.Inspect()

	f.Remove(rightNow + 20)
	f.Inspect()
}

/*
Feed Length:  5
Item: 0 - &{Lorem ipsum 1680669912 0x140000b4020}
Item: 1 - &{Dolor sit amet 1680669922 0x140000b4080}
Item: 2 - &{This is a new post 1680669927 0x140000b4040}
Item: 3 - &{consectetur adipiscing elit 1680669932 0x140000b4060}
Item: 4 - &{sed do eiusmod tempor incididunt 1680669942 <nil>}
Feed Length:  4
Item: 0 - &{Lorem ipsum 1680669912 0x140000b4020}
Item: 1 - &{Dolor sit amet 1680669922 0x140000b4080}
Item: 2 - &{This is a new post 1680669927 0x140000b4060}
Item: 3 - &{sed do eiusmod tempor incididunt 1680669942 <nil>}
*/
