package test

import (
	"fmt"
	"testing"

	model "gitlab.com/citaces/go-kata/module3/clean_architecture/serviceTask/model"
	repo "gitlab.com/citaces/go-kata/module3/clean_architecture/serviceTask/repo"
)

func TestGetTask(t *testing.T) {
	rep := &repo.TodoRepository{}
	rep.Tasks = []model.Todo{{ID: 1, Title: "title1", Description: "descr1", Status: "not completed"},
		{ID: 2, Title: "title2", Description: "descr2", Status: "not completed"},
		{ID: 3, Title: "title2", Description: "descr2", Status: "not completed"}}
	findTask, err := rep.GetTask(2)
	if err != nil {
		panic(err)
	}
	s := fmt.Sprintf("%v", findTask)
	expectingGetTask(t, s)
}

func TestGetTasks(t *testing.T) {
	rep := &repo.TodoRepository{}
	rep.Tasks = []model.Todo{{ID: 1, Title: "title1", Description: "descr1", Status: "not completed"},
		{ID: 2, Title: "title2", Description: "descr2", Status: "not completed"},
		{ID: 3, Title: "title2", Description: "descr2", Status: "not completed"}}
	findTasks, err := rep.GetTasks()
	if err != nil {
		panic(err)
	}
	s := fmt.Sprintf("%v", findTasks)
	expectingGetTasks(t, s)
}

func TestDeleteTask(t *testing.T) {
	rep := &repo.TodoRepository{}
	rep.Tasks = []model.Todo{{ID: 1, Title: "title1", Description: "descr1", Status: "not completed"},
		{ID: 2, Title: "title2", Description: "descr2", Status: "not completed"},
		{ID: 3, Title: "title2", Description: "descr2", Status: "not completed"}}
	err := rep.DeleteTask(2)
	if err != nil {
		panic(err)
	}
	expectingDeleteTask(t, len(rep.Tasks))
}

func TestUpdateTask(t *testing.T) {
	rep := &repo.TodoRepository{}
	rep.Tasks = []model.Todo{{ID: 1, Title: "title1", Description: "descr1", Status: "not completed"},
		{ID: 2, Title: "title2", Description: "descr2", Status: "not completed"},
		{ID: 3, Title: "title2", Description: "descr2", Status: "not completed"}}
	_, err := rep.UpdateTask(model.Todo{ID: 2, Title: "UPDATED", Description: "UPDATED", Status: "COMPLETED"})
	if err != nil {
		panic(err)
	}
	expectingUpdateTask(t, rep.Tasks[1].Title)
}

func expectingGetTask(t *testing.T, s string) {
	if s != "{2 title2 descr2 not completed}" {
		t.Fail()
	}
}

func expectingGetTasks(t *testing.T, s string) {
	if s != "[{1 title1 descr1 not completed} {2 title2 descr2 not completed} {3 title2 descr2 not completed}]" {
		t.Fail()
	}
}

func expectingDeleteTask(t *testing.T, length int) {
	if length != 2 {
		t.Fail()
	}
}

func expectingUpdateTask(t *testing.T, title string) {
	if title != "UPDATED" {
		t.Fail()
	}
}
