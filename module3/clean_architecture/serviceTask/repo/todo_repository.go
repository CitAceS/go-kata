package repo

import (
	"errors"

	model "gitlab.com/citaces/go-kata/module3/clean_architecture/serviceTask/model"
)

type Repository interface {
	GetTask(id int) (model.Todo, error)
	GetTasks() ([]model.Todo, error)
	CreateTask(task model.Todo) error
	SaveTasks(task []model.Todo) error
	UpdateTask(task model.Todo) (model.Todo, error)
	DeleteTask(id int) error
	CompleteTask(id int) error
}

type TodoRepository struct {
	Tasks []model.Todo
}

func NewTodoRepository() *TodoRepository {
	return &TodoRepository{
		Tasks: make([]model.Todo, 0),
	}
}

func (repo *TodoRepository) GetTasks() ([]model.Todo, error) {
	tasks := make([]model.Todo, len(repo.Tasks))
	copy(tasks, repo.Tasks)
	return tasks, nil
}

func (repo *TodoRepository) UpdateTask(task model.Todo) (model.Todo, error) {
	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}
	found := false
	for i, t := range tasks {
		if t.ID == task.ID {
			found = true
			tasks[i] = task
			break
		}
	}
	if found {
		if err := repo.SaveTasks(tasks); err != nil {
			return task, err
		}
		return task, nil
	} else {
		return task, errors.New("task with this id not found")
	}
}

func (repo *TodoRepository) CreateTask(task model.Todo) error {
	if len(repo.Tasks) != 0 {
		task.ID = repo.Tasks[len(repo.Tasks)-1].ID + 1
	} else {
		task.ID = 1
	}
	task.Status = "not completed"
	repo.Tasks = append(repo.Tasks, task)
	return nil
}

func (repo *TodoRepository) GetTask(id int) (model.Todo, error) {
	tasks, err := repo.GetTasks()
	if err != nil {
		return model.Todo{}, err
	}
	for _, task := range tasks {
		if task.ID == id {
			return task, nil
		}
	}
	return model.Todo{}, errors.New("task with this id not found")
}

func (repo *TodoRepository) SaveTasks(tasks []model.Todo) error {
	repo.Tasks = tasks
	return nil
}

func (repo *TodoRepository) DeleteTask(id int) error {
	task, err := repo.GetTask(id)
	if err != nil {
		return err
	}
	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}
	if len(tasks) > 1 {
		a, b := tasks[:task.ID-1], tasks[task.ID:]
		tasks = append(a, b...)
	} else {
		tasks = make([]model.Todo, 0)
	}
	err = repo.SaveTasks(tasks)
	if err != nil {
		return err
	}

	return nil
}

func (repo *TodoRepository) CompleteTask(id int) error {
	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}
	found := false
	for i, t := range tasks {
		if t.ID == id {
			found = true
			tasks[i].Status = "completed!"
			break
		}
	}
	if found {
		if err := repo.SaveTasks(tasks); err != nil {
			return err
		}
		return nil
	} else {
		return errors.New("task with this id not found")
	}
}
