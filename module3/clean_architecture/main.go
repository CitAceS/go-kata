package main

import (
	"gitlab.com/citaces/go-kata/module3/clean_architecture/cliTask"
	"gitlab.com/citaces/go-kata/module3/clean_architecture/serviceTask/repo"
	"gitlab.com/citaces/go-kata/module3/clean_architecture/serviceTask/service"
)

func main() {
	cli := &cliTask.TodoCLI{Service: service.NewTodoService(repo.NewTodoRepository())}
	cli.Run()
}
