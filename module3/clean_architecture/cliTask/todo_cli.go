package cliTask

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"

	"gitlab.com/citaces/go-kata/module3/clean_architecture/serviceTask/model"
	"gitlab.com/citaces/go-kata/module3/clean_architecture/serviceTask/service"
)

type CLI interface {
	DisplayMenu()
	ShowTasks()
	AddTask()
	RemoveTask()
	EditTask()
	CompleteTask()
	Run()
}

type TodoCLI struct {
	Service service.Service
}

func NewCLI(ser *service.Service) *TodoCLI {
	return &TodoCLI{Service: *ser}
}

func (cli *TodoCLI) DisplayMenu() {
	fmt.Println("1 - Показать список задач")
	fmt.Println("2 - Добавить задачу")
	fmt.Println("3 - Удалить задачу")
	fmt.Println("4 - Редактировать задачу по ID")
	fmt.Println("5 - Завершить задачу")
	fmt.Println("1 - Выход из приложения")
}

func (cli *TodoCLI) ShowTasks() {
	tasks, err := cli.Service.ListTodos()
	if err != nil {
		panic(err)
	}
	if len(tasks) == 0 {
		fmt.Println("task list is empty")
	} else {
		for _, task := range tasks {
			fmt.Printf("%d. %s - %s %s\n", task.ID, task.Title, task.Description, task.Status)
		}
	}
}

func (cli *TodoCLI) AddTask() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Write a title: ")
	title, _ := reader.ReadString('\n')
	title = strings.TrimSuffix(title, "\n")
	fmt.Print("Write a description: ")
	description, _ := reader.ReadString('\n')
	description = strings.TrimSuffix(description, "\n")
	err := cli.Service.CreateTodo(model.Todo{Title: title, Description: description})
	if err != nil {
		panic(err)
	}
	fmt.Println("New task added successfully!")
}

func (cli *TodoCLI) RemoveTask() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Task ID to remove: ")
	id, _ := reader.ReadString('\n')
	id = strings.TrimSuffix(id, "\n")
	taskID, err := strconv.Atoi(id)
	if err != nil {
		fmt.Println(errors.New("wrong id"))
		return
	}
	result := cli.Service.RemoveTodo(model.Todo{ID: taskID})
	if result != nil {
		fmt.Println("Task not found")
	} else {
		fmt.Println("Task is successfully removed from TODO-list!")
	}
}

func (cli *TodoCLI) EditTask() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Task ID to edit: ")
	id, _ := reader.ReadString('\n')
	id = strings.TrimSuffix(id, "\n")
	taskID, err := strconv.Atoi(id)
	if err != nil {
		fmt.Println(errors.New("wrong id"))
		return
	}
	fmt.Print("Write a new title: ")
	title, _ := reader.ReadString('\n')
	title = strings.TrimSuffix(title, "\n")
	fmt.Print("Write a new description: ")
	description, _ := reader.ReadString('\n')
	description = strings.TrimSuffix(description, "\n")
	result := cli.Service.EditTodo(model.Todo{ID: taskID, Title: title, Description: description, Status: "not completed"})
	if result != nil {
		fmt.Println("Task not found")
	} else {
		fmt.Println("Task is successfully updated!")
	}
}

func (cli *TodoCLI) CompleteTask() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Task ID to edit: ")
	id, _ := reader.ReadString('\n')
	id = strings.TrimSuffix(id, "\n")
	taskID, err := strconv.Atoi(id)
	if err != nil || taskID == 0 {
		fmt.Println(errors.New("wrong id"))
		return
	}
	result := cli.Service.CompleteTodo(taskID)
	if result != nil {
		fmt.Println("Task not found")
	} else {
		fmt.Println("Status successfully updated!")
	}
}

func (cli *TodoCLI) Run() {
	for {
		cli.DisplayMenu()
		fmt.Println("TODO приложение приветствует Вас!")
		fmt.Print("Сделайте свой выбор: ")
		reader := bufio.NewReader(os.Stdin)
		choice, _ := reader.ReadString('\n')
		choice = strings.TrimSuffix(choice, "\n")

		switch choice {
		case "1":
			cli.ShowTasks()
		case "2":
			cli.AddTask()
		case "3":
			cli.RemoveTask()
		case "4":
			cli.EditTask()
		case "5":
			cli.CompleteTask()
		case "6":
			fmt.Println("Exit...")
			return
		default:
			fmt.Println("No such option. Try again.")
		}
	}
}
