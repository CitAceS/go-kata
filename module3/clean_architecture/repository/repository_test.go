package main

import (
	"fmt"
	"os"
	"testing"
)

func TestUserRepository_Find(t *testing.T) {
	rep := &UserRepository{}
	rep.Users = []User{{ID: 1, Name: "Hellmuth"}, {ID: 2, Name: "TonyG"}, {ID: 3, Name: "Dwan"}}
	findID, err := rep.Find(2)
	if err != nil {
		panic(err)
	}
	s := fmt.Sprintf("%v", findID)
	expectingFind(t, s)
}

func TestUserRepository_FindAll(t *testing.T) {
	rep := &UserRepository{}
	rep.Users = []User{{ID: 1, Name: "Hellmuth"}, {ID: 2, Name: "TonyG"}, {ID: 3, Name: "Dwan"}}
	fAll, err := rep.FindAll()
	if err != nil {
		panic(err)
	}
	s := fmt.Sprintf("%v", fAll)
	expectingFindAll(t, s)
}

func TestUserRepository_Save(t *testing.T) {
	data, err := os.OpenFile("./repo.json", os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		panic(err)
	}
	defer data.Close()

	rep := NewUserRepository(data)
	rep.Users = []User{{ID: 1, Name: "Hellmuth"}, {ID: 2, Name: "TonyG"}, {ID: 3, Name: "Dwan"}}
	additional := User{Name: "Moneymaker"}
	err = rep.Save(additional)
	if err != nil {
		panic(err)
	}

	expectingSave(t, len(rep.Users))
}

func expectingFind(t *testing.T, s string) {
	if s != "{2 TonyG}" {
		t.Fail()
	}
}

func expectingFindAll(t *testing.T, s string) {
	if s != "[{1 Hellmuth} {2 TonyG} {3 Dwan}]" {
		t.Fail()
	}
}

func expectingSave(t *testing.T, length int) {
	if length != 4 {
		t.Fail()
	}
}
