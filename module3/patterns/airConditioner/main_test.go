package main

import (
	"testing"
)

func TestAirConditionerProxy_Auth(t *testing.T) {
	admin := NewAirConditionerProxy(true)
	if admin.adapter.airConditioner.status {
		t.Fail()
	}
	admin.on()
	if !admin.adapter.airConditioner.status {
		t.Fail()
	}
	admin.set(5)
	if admin.adapter.airConditioner.temperature != 5 {
		t.Fail()
	}
}

func TestAirConditionerProxy_Not_Auth(t *testing.T) {
	admin := NewAirConditionerProxy(false)
	if admin.adapter.airConditioner.status {
		t.Fail()
	}
	admin.on()
	if admin.adapter.airConditioner.status {
		t.Fail()
	}
	admin.set(5)
	if admin.adapter.airConditioner.temperature != 0 {
		t.Fail()
	}
}
