package main

import (
	"fmt"
	"strings"
)

/*
23. Интерпретация синтаксического анализатора цели
У вас есть синтаксический анализатор целей, который может интерпретировать строковую команду.
Команда состоит из алфавита "G", "()" и/или "(al)" в некотором порядке. Анализатор целей будет интерпретировать "G" как
строку "G", "()" как строку "o" и "(al)" как строку "al". Затем интерпретируемые строки объединяются в исходном порядке.
Учитывая строковую команду, вернуть интерпретацию команды синтаксического анализатора целей.
Пример 1:
Input: command = "G()(al)"
Output: "Goal"
Объяснение: Синтаксический анализатор целей интерпретирует команду следующим образом:
G -> G
() -> o
(al) -> al
Конечным объединенным результатом является "Goal".
Пример 2:
Input: command = "G()()()()(al)"
Output: "Gooooal"
Example 3:
Input: command = "(al)G(al)()()G"
Output: "alGalooG"
Ограничения:
1 <= command.length <= 100
command состоит из "G", "()", и/или "(al)" в некотором порядке.
https://leetcode.com/problems/goal-parser-interpretation/
*/

func interpret(command string) string {
	s1 := strings.ReplaceAll(command, "()", "o")
	s2 := strings.ReplaceAll(s1, "(al)", "al")
	return s2
}

func main() {
	fmt.Println(interpret("G()(al)"))
	fmt.Println(interpret("G()()()()(al)"))
}
