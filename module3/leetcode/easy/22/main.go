package main

import "fmt"

/*
22. Сколько чисел меньше, чем текущее число
Дан массив nums. Для каждого nums [i] выясните, сколько чисел в массиве меньше его. То есть, для каждого nums [i] вам нужно посчитать количество допустимых j, таких что j != i и nums [j] <nums [i].
Верните ответ в виде массива.
Пример 1:
Input: nums = [8,1,2,2,3]
Output: [4,0,1,1,3]
Объяснение:
Для nums [0] = 8 существует четыре меньших числа, чем оно (1, 2, 2 и 3).
Для nums [1] = 1 не существует меньшего числа, чем оно.
Для nums [2] = 2 существует одно меньшее число, чем оно (1).
Для nums [3] = 2 существует одно меньшее число, чем оно (1).
Для nums [4] = 3 существует три меньших числа, чем оно (1, 2 и 2).
Пример 2:
Input: nums = [6,5,4,8]
Output: [2,1,0,3]
Пример 3:
Input: nums = [7,7,7,7]
Output: [0,0,0,0]
Ограничения:
2 <= nums.length <= 500
0 <= nums [i] <= 100
https://leetcode.com/problems/how-many-numbers-are-smaller-than-the-current-number/
*/

func smallerNumbersThanCurrent(nums []int) []int {
	sl := make([]int, 0, len(nums))
	count := 0
	for i := 0; i < len(nums); i++ {
		count = 0
		for _, elem := range nums {
			if nums[i] > elem {
				count++
			}
		}
		sl = append(sl, count)
	}
	return sl
}

func main() {
	fmt.Println(smallerNumbersThanCurrent([]int{8, 1, 2, 2, 3}))
}
