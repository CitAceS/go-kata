package main

import (
	"fmt"
	"strings"
)

/*
17. Максимальное количество найденных слов в предложениях
«Предложение» — это список слов, разделенных одним пробелом, без ведущих или завершающих пробелов.
Вам дается массив строк sentences, где sentences[i] представляет одно предложение.
Верните максимальное количество слов, которые появляются в одном предложении.
Пример 1:
Input: sentences = ["alice and bob love leetcode", "i think so too", "this is great thanks very much"]
Output: 6
Объяснение:
Первое предложение, "alice and bob love leetcode", всего имеет 5 слов.
Второе предложение, "i think so too", всего имеет 4 слова.
Третье предложение, "this is great thanks very much", всего имеет 6 слов.
Таким образом, максимальное количество слов в одном предложении есть в третьем предложении, которое имеет 6 слов.
Пример 2:
Input: sentences = ["please wait", "continue to fight", "continue to win"]
Output: 3
Объяснение: Возможно, несколько предложений содержат одинаковое количество слов.
В этом примере второе и третье предложение (подчеркнутые) имеют одинаковое количество слов.
Ограничения:
1 <= sentences.length <= 100
1 <= sentences[i].length <= 100
sentences[i] состоит только из строчных английских букв и только ' '.
sentences[i] не имеет ведущих или завершающих пробелов.
Все слова в sentences[i] разделены одинарным пробелом.
https://leetcode.com/problems/maximum-number-of-words-found-in-sentences/
*/

func mostWordsFound(sentences []string) (count int) {
	for _, elem := range sentences {
		s := strings.Fields(elem)
		if count < len(s) {
			count = len(s)
		}
	}
	return count
}

func main() {
	fmt.Println(mostWordsFound([]string{"alice and bob love leetcode", "i think so too", "this is great thanks very much"}))
}
