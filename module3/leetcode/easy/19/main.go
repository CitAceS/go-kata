package main

import (
	"fmt"
	"sort"
	"strconv"
)

/*
19. Минимальная сумма четырехзначного числа после разделения цифр
Дано положительное четырехзначное число num. Разделите num на два новых числа new1 и new2,
используя цифры, найденные в num. В new1 и new2 разрешены ведущие нули, и все цифры, найденные в num, должны быть использованы.
Например, дано num = 2932, у вас есть следующие цифры: две 2, одна 9 и одна 3. Некоторые из возможных пар
[new1, new2] это [22, 93], [23, 92], [223, 9] и [2, 329].
Вернуть минимально возможную сумму new1 и new2.
Пример 1:
Input: num = 2932
Output: 52
Объяснение: Некоторые возможные пары [new1, new2] - [29, 23], [223, 9] и т.д. Минимальная сумма может быть получена парой [29, 23]: 29 + 23 = 52.
Пример 2:
Input: num = 4009
Output: 13
Объяснение: Некоторые возможные пары [new1, new2] - [0, 49], [490, 0] и т.д.
Минимальная сумма может быть получена парой [4, 9]: 4 + 9 = 13.
Ограничения:
1000 <= num <= 9999
https://leetcode.com/problems/minimum-sum-of-four-digit-number-after-splitting-digits/
*/
func minimumSum(num int) int {
	sl := make([]int, 0, 4)
	for i := num; i > 0; i /= 10 {
		sl = append(sl, i%10)
	}
	sort.Ints(sl)
	s1 := strconv.Itoa(sl[0])
	s2 := strconv.Itoa(sl[1])
	s3 := strconv.Itoa(sl[2])
	s4 := strconv.Itoa(sl[3])
	n1, _ := strconv.Atoi(s1 + s3)
	n2, _ := strconv.Atoi(s2 + s4)
	return n1 + n2
}

func main() {
	fmt.Println(minimumSum(2932))
	fmt.Println(minimumSum(4009))
}
