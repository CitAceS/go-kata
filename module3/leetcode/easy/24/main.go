package main

import "fmt"

/*
24. Декодирование XOR-массива
Есть скрытый массив целых чисел arr, состоящий из n неотрицательных целых чисел.
Он был закодирован в другой массив encoded длиной n - 1, так что encoded [i] = arr [i] XOR arr [i + 1]. Например, если arr = [1,0,2,1], то encoded = [1,2,3].
Вам дан закодированный массив. Вам также дано целое число first, это первый элемент arr, т.е. arr [0].
Верните исходный массив arr. Может быть доказано, что ответ существует и является уникальным.
Пример 1:
Input: encoded = [1,2,3], first = 1
Output: [1,0,2,1]
Объяснение: Если arr = [1,0,2,1], то first = 1 и encoded = [1 XOR 0, 0 XOR 2, 2 XOR 1] = [1,2,3]
Пример 2:
Input: encoded = [6,2,7,3], first = 4
Output: [4,2,0,7,4]
Ограничения:
2 <= n <= 104
encoded.length == n - 1
0 <= encoded[i] <= 105
0 <= first <= 105
*/

func decode(encoded []int, first int) []int {
	sl := make([]int, 0, len(encoded)+1)
	sl = append(sl, first)
	for i, elem := range encoded {
		sl = append(sl, sl[i]^elem)
	}
	return sl
}

func main() {
	fmt.Println(decode([]int{6, 2, 7, 3}, 4))
}
