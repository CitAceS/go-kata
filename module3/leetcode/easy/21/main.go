package main

import "fmt"

/*
21. Разница между произведением и суммой цифр целого числа
Дано целое число n. Верните разницу между произведением его цифр и суммой его цифр.
Пример 1:
Input: n = 234
Output: 15
Объяснение:
Произведение цифр = 2 * 3 * 4 = 24
Сумма цифр = 2 + 3 + 4 = 9
Результат = 24 - 9 = 15
Пример 2:
Input: n = 4421
Output: 21
Объяснение:
Произведение цифр = 4 * 4 * 2 * 1 = 32
Сумма цифр = 4 + 4 + 2 + 1 = 11
Результат = 32 - 11 = 21
Ограничения:
1 <= n <= 10^5
https://leetcode.com/problems/subtract-the-product-and-sum-of-digits-of-an-integer/
*/

func subtractProductAndSum(n int) int {
	p, s := 1, 0
	for i := n; i > 0; i /= 10 {
		p *= i % 10
		s += i % 10
	}
	return p - s
}

func main() {
	fmt.Println(subtractProductAndSum(4421))
}
