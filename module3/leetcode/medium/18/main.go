package main

import "fmt"

/*
18. Сгруппируйте людей в соответствии с размером их группы.
Есть n людей, которые разделены на неизвестное количество групп. Каждый человек имеет уникальный идентификатор от 0 до n-1.
Вам дан массив целых чисел groupSizes, где groupSizes [i] - это размер группы, в которой находится человек i. Например, если groupSizes [1] = 3, то человек 1 должен находиться в группе размером 3.
Верните список групп таким образом, чтобы каждый человек i находился в группе размера groupSizes [i].
Каждый человек должен появиться ровно в одной группе, и каждый человек должен быть в группе. Если есть несколько ответов, верните любой из них. Гарантируется, что для данного входного значения будет по крайней мере одно допустимое решение.
Пример 1:
Input: groupSizes = [3,3,3,3,3,1,3]
Output: [[5],[0,1,2],[3,4,6]]
Объяснение:
Первая группа - [5]. Размер равен 1, а groupSizes [5] = 1.
Вторая группа - [0,1,2]. Размер равен 3, а groupSizes [0] = groupSizes [1] = groupSizes [2] = 3.
Третья группа - [3,4,6]. Размер равен 3, а groupSizes [3] = groupSizes [4] = groupSizes [6] = 3.
Другие возможные решения - [[2,1,6], [5], [0,4,3]] и [[5], [0,6,2], [4,3,1]].
Пример 2:
Input: groupSizes = [2,1,3,3,3,2]
Output: [[1],[0,5],[2,3,4]]
Constraints:
groupSizes.length == n
1 <= n <= 500
1 <= groupSizes[i] <= n
https://leetcode.com/problems/group-the-people-given-the-group-size-they-belong-to/
*/

func groupThePeople(groupSizes []int) [][]int {
	test := make([][]int, len(groupSizes))
	res := make([][]int, 0, len(test))
	grp := make(map[int]int, len(groupSizes))
	for i, e := range groupSizes {
		grp[i] = e
	}
	fmt.Println(grp)
	for i := 0; i < len(groupSizes); i++ {
		if val, ok := grp[i]; ok {
			if val > len(test[val-1]) { //overflows
				test[val-1] = append(test[val-1], i)
			} else if val == len(test[val-1]) {
				res = append(res, test[val-1])
				test[val-1] = nil
				test[val-1] = append(test[val-1], i)
			}
		}
	}

	for _, el := range test {
		if len(el) != 0 {
			res = append(res, el)
		}
	}
	return res
}

func main() {
	fmt.Println(groupThePeople([]int{3, 4, 3, 3, 4, 4, 3, 4, 3, 3})) // [[0 2 3] [1 4 5 7] [6 8 9]]
	fmt.Println(groupThePeople([]int{2, 2, 1, 1, 1, 1, 1, 1}))       //
	//fmt.Println(groupThePeople([]int{3, 3, 3, 3, 3, 1, 3}))
	//fmt.Println(groupThePeople([]int{2, 1, 3, 3, 3, 2}))
}
