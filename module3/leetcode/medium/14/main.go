package main

import "fmt"

/*
14. Запросы XOR подмассива
Вам дан массив arr положительных целых чисел. Вам также дан массив запросов, где queries [i] = [lefti, righti].
Для каждого запроса i вычислите XOR элементов от lefti до righti (то есть, arr [lefti] XOR arr [lefti + 1] XOR ... XOR arr [righti]).
Верните массив ответов, где answer [i] является ответом на ith-запрос.
Пример 1:
Input: arr = [1,3,4,8], queries = [[0,1],[1,2],[0,3],[3,3]]
Output: [2,7,14,8]
Объяснение:
Бинарное представление элементов массива:
1 = 0001
3 = 0011
4 = 0100
8 = 1000
Значения XOR для запросов:
[0,1] = 1 xor 3 = 2
[1,2] = 3 xor 4 = 7
[0,3] = 1 xor 3 xor 4 xor 8 = 14
[3,3] = 8
Example 2:
Input: arr = [4,8,2,10], queries = [[2,3],[1,3],[0,0],[0,3]]
Output: [8,0,4,4]
Constraints:
1 <= arr.length, queries.length <= 3 * 104
1 <= arr[i] <= 109
queries[i].length == 2
0 <= lefti <= righti < arr.length
https://leetcode.com/problems/xor-queries-of-a-subarray/
*/

func main() {
	fmt.Println(xorQueries([]int{1, 3, 4, 8}, [][]int{{0, 1}, {1, 2}, {0, 3}, {3, 3}}))
	fmt.Println(xorQueries([]int{4, 8, 2, 10}, [][]int{{2, 3}, {1, 3}, {0, 0}, {0, 3}}))
}

func xorQueries(arr []int, queries [][]int) []int {
	res := make([]int, 0, len(queries))
	for _, q := range queries {
		count := 0
		for _, qs := range arr[q[0] : q[1]+1] {
			count ^= qs
		}
		res = append(res, count)
	}
	return res
}
