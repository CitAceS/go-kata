package main

import "testing"

func Benchmark_minPartitions(b *testing.B) {
	for i := 0; i < b.N; i++ {
		minPartitions(TS)
	}
}

func Test_minPartitions(t *testing.T) {
	type args struct {
		n string
	}
	tests := []struct {
		name string
		args args
		want int
	}{{
		name: "testS",
		args: args{TS},
		want: 6,
	},
	// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := minPartitions(tt.args.n); got != tt.want {
				t.Errorf("minPartitions() = %v, want %v", got, tt.want)
			}
		})
	}
}
