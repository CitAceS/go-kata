package main

import (
	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/controllers"
	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/handlers"
	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/repository"
	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/service"
)

func main() {

	petRepo := repository.NewPetStorage()
	petServ := service.NewPetServ(petRepo)
	petContr := controllers.NewPetControl(petServ)

	storeRepo := repository.NewStoreStorage()
	storeServ := service.NewStoreServ(storeRepo)
	storeContr := controllers.NewStoreControl(storeServ)

	usersRepo := repository.NewUserStorage()
	usersServ := service.NewUserServ(usersRepo)
	usersContr := controllers.NewUserControl(usersServ)

	hand := handlers.NewHandlers(petContr, storeContr, usersContr)
	hand.Route()
}
