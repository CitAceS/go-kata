package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"

	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/service"

	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"
)

type StoreController interface {
	StoreCreate(w http.ResponseWriter, r *http.Request)
	StoreDel(w http.ResponseWriter, r *http.Request)
	StoreGetByID(w http.ResponseWriter, r *http.Request)
	StoreGetByStatus(w http.ResponseWriter, r *http.Request)
}

type StoreControl struct {
	service service.StoreService
}

func NewStoreControl(service service.StoreService) *StoreControl {
	return &StoreControl{service}
}

func (s *StoreControl) StoreCreate(w http.ResponseWriter, r *http.Request) {
	var store model.Store
	err := json.NewDecoder(r.Body).Decode(&store)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	store = s.service.StoreCreate(store)

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(store)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *StoreControl) StoreDel(w http.ResponseWriter, r *http.Request) {
	i := chi.URLParam(r, "storeID")
	id, err := strconv.Atoi(i)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	stores, err := s.service.StoreDelId(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(stores)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *StoreControl) StoreGetByID(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		store      model.Store
		err        error
		storeIDRaw string
		storeID    int
	)

	storeIDRaw = chi.URLParam(r, "storeID") // получаем petID из chi router

	storeID, err = strconv.Atoi(storeIDRaw) // конвертируем в int
	if err != nil {                         // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	store, err = s.service.StoreGetById(storeID) // пытаемся получить Pet по id
	if err != nil {                              // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(store)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (s *StoreControl) StoreGetByStatus(w http.ResponseWriter, r *http.Request) {

	store := s.service.StoreGetByStatus()
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(store)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}
