{
  "consumes": [
    "application/json",
    "multipart/form-data"
  ],
  "produces": [
    "application/json"
  ],
  "swagger": "2.0",
  "info": {
    "description": "Documentation of your project API.\n\nSchemes:\nhttp\nhttps",
    "title": "infoblog.",
    "version": "1.0.0"
  },
  "basePath": "/",
  "paths": {
    "/pet": {
      "post": {
        "tags": [
          "pet"
        ],
        "summary": "Add a new pet to the store.",
        "operationId": "petAddRequest",
        "parameters": [
          {
            "description": "Pet object that needs to be added to the store",
            "name": "Body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Pet"
            }
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/petAddResponse"
          }
        }
      }
    },
    "/pet/findByStatus": {
      "get": {
        "description": "Multiple status values can be provided with comma separated strings",
        "tags": [
          "pet"
        ],
        "summary": "Finds Pets by status.",
        "operationId": "petGetByStatusRequest",
        "parameters": [
          {
            "type": "array",
            "items": {
              "enum": [
                "available",
                " pending",
                " sold"
              ],
              "type": "string"
            },
            "x-go-name": "Status",
            "description": "Status values that need to be considered for filter",
            "name": "status",
            "in": "query",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/petGetByStatusResponse"
          }
        }
      }
    },
    "/pet/listPets": {
      "get": {
        "tags": [
          "pet"
        ],
        "summary": "List Pets.",
        "operationId": "petListRequest",
        "responses": {
          "200": {
            "$ref": "#/responses/petListResponse"
          }
        }
      }
    },
    "/pet/{id}": {
      "get": {
        "description": "Returns a single pet",
        "tags": [
          "pet"
        ],
        "summary": "Find pet by ID.",
        "operationId": "petGetByIDRequest",
        "parameters": [
          {
            "type": "string",
            "x-go-name": "ID",
            "description": "ID of pet to return",
            "name": "id",
            "in": "path",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/petGetByIDResponse"
          }
        }
      },
      "put": {
        "tags": [
          "pet"
        ],
        "summary": "Updates an existing pet.",
        "operationId": "petUpdRequest",
        "parameters": [
          {
            "type": "string",
            "x-go-name": "ID",
            "description": "Pet object that needs to be added to the store",
            "name": "id",
            "in": "path",
            "required": true
          },
          {
            "name": "Body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Pet"
            }
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/petUpdResponse"
          }
        }
      },
      "post": {
        "tags": [
          "pet"
        ],
        "summary": "Updates a pet in the store with form data.",
        "operationId": "petUpdIdRequest",
        "parameters": [
          {
            "type": "integer",
            "format": "int64",
            "x-go-name": "ID",
            "description": "ID of pet that needs to be updated",
            "name": "id",
            "in": "path",
            "required": true
          },
          {
            "type": "string",
            "x-go-name": "Name",
            "description": "ID of pet that needs to be updated",
            "name": "name",
            "in": "formData"
          },
          {
            "type": "string",
            "x-go-name": "Status",
            "description": "ID of pet that needs to be updated",
            "name": "status",
            "in": "formData"
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/petUpdIdResponse"
          }
        }
      },
      "delete": {
        "tags": [
          "pet"
        ],
        "summary": "Del a pet from the store.",
        "operationId": "petDelRequest",
        "parameters": [
          {
            "type": "string",
            "x-go-name": "API",
            "name": "api_key",
            "in": "header"
          },
          {
            "type": "integer",
            "format": "int64",
            "x-go-name": "ID",
            "name": "id",
            "in": "path",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/petDelResponse"
          }
        }
      }
    },
    "/pet/{id}/uploadImage": {
      "post": {
        "tags": [
          "pet"
        ],
        "summary": "Uploads an image.",
        "operationId": "uploadPetPhotoRequest",
        "parameters": [
          {
            "type": "integer",
            "format": "int64",
            "x-go-name": "ID",
            "description": "The ID of the pet to upload a photo for",
            "name": "id",
            "in": "path",
            "required": true
          },
          {
            "type": "file",
            "x-go-name": "File",
            "description": "The file to upload",
            "name": "file",
            "in": "formData",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": ""
          }
        }
      }
    },
    "/store/inventory": {
      "get": {
        "description": "Returns a map of status codes to quantities",
        "tags": [
          "store"
        ],
        "summary": "Returns pet inventories by status.",
        "operationId": "storeGetByStatusRequest",
        "responses": {
          "200": {
            "$ref": "#/responses/storeGetByStatusResponse"
          }
        }
      }
    },
    "/store/order": {
      "post": {
        "tags": [
          "store"
        ],
        "summary": "Place an order for a pet.",
        "operationId": "storePlaceAnOrderRequest",
        "parameters": [
          {
            "description": "order placed for purchasing the pet",
            "name": "Body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Store"
            }
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/storePlaceAnOrderResponse"
          }
        }
      }
    },
    "/store/order/{id}": {
      "get": {
        "tags": [
          "store"
        ],
        "summary": "Find purchase order by ID.",
        "operationId": "storeGetByIDRequest",
        "parameters": [
          {
            "type": "integer",
            "format": "int64",
            "x-go-name": "ID",
            "name": "id",
            "in": "path",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/storeGetByIDResponse"
          }
        }
      },
      "delete": {
        "tags": [
          "store"
        ],
        "summary": "Delete purchase order by ID.",
        "operationId": "storeDeleteByIdRequest",
        "parameters": [
          {
            "type": "integer",
            "format": "int64",
            "x-go-name": "OrderID",
            "description": "For valid response try integer IDs with positive integer value. Negative or non-integer values will generate API errors\n\nID of the order that needs to be deleted",
            "name": "id",
            "in": "path",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/storeDeleteByIdResponse"
          }
        }
      }
    },
    "/user": {
      "post": {
        "tags": [
          "user"
        ],
        "summary": "Create user.",
        "operationId": "userCreateRequest",
        "parameters": [
          {
            "description": "Created user object",
            "name": "Body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/User"
            }
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/userCreateResponse"
          }
        }
      }
    },
    "/user/createWithList": {
      "post": {
        "tags": [
          "user"
        ],
        "summary": "Creates list of users with given input array.",
        "operationId": "userCreateWithListRequest",
        "parameters": [
          {
            "description": "List of user object",
            "name": "Body",
            "in": "body",
            "required": true,
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/User"
              }
            }
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/userCreateWithListResponse"
          }
        }
      }
    },
    "/user/{username}": {
      "get": {
        "description": "Get user by user name",
        "tags": [
          "user"
        ],
        "operationId": "userGetByUsernameRequest",
        "parameters": [
          {
            "type": "string",
            "x-go-name": "Username",
            "description": "The name that needs to be fetched",
            "name": "username",
            "in": "path",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/userGetByUsernameResponse"
          }
        }
      },
      "put": {
        "tags": [
          "user"
        ],
        "summary": "Update user.",
        "operationId": "userUpdRequest",
        "parameters": [
          {
            "type": "string",
            "x-go-name": "Username",
            "description": "name that need to be updated",
            "name": "username",
            "in": "path",
            "required": true
          },
          {
            "name": "Body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/User"
            }
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/userUpdResponse"
          }
        }
      },
      "delete": {
        "tags": [
          "user"
        ],
        "summary": "Delete user.",
        "operationId": "userDeleteByUserNameRequest",
        "parameters": [
          {
            "type": "string",
            "x-go-name": "Username",
            "description": "The name that needs to be deleted",
            "name": "username",
            "in": "path",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/userDeleteByUserNameResponse"
          }
        }
      }
    }
  },
  "definitions": {
    "Category": {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int64",
          "x-go-name": "ID"
        },
        "name": {
          "type": "string",
          "x-go-name": "Name"
        }
      },
      "x-go-package": "gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"
    },
    "Pet": {
      "type": "object",
      "properties": {
        "category": {
          "$ref": "#/definitions/Category"
        },
        "id": {
          "type": "integer",
          "format": "int64",
          "x-go-name": "ID"
        },
        "name": {
          "type": "string",
          "x-go-name": "Name"
        },
        "photoUrls": {
          "type": "array",
          "items": {
            "type": "string"
          },
          "x-go-name": "PhotoUrls"
        },
        "status": {
          "type": "string",
          "x-go-name": "Status"
        },
        "tags": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Category"
          },
          "x-go-name": "Tags"
        }
      },
      "x-go-package": "gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"
    },
    "Store": {
      "type": "object",
      "properties": {
        "complete": {
          "type": "boolean",
          "x-go-name": "Complete"
        },
        "id": {
          "type": "integer",
          "format": "int64",
          "x-go-name": "ID"
        },
        "petId": {
          "type": "integer",
          "format": "int64",
          "x-go-name": "PetID"
        },
        "quantity": {
          "type": "integer",
          "format": "int64",
          "x-go-name": "Quantity"
        },
        "shipDate": {
          "type": "string",
          "x-go-name": "ShipDate"
        },
        "status": {
          "type": "string",
          "x-go-name": "Status"
        }
      },
      "x-go-package": "gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"
    },
    "User": {
      "type": "object",
      "properties": {
        "email": {
          "type": "string",
          "x-go-name": "Email"
        },
        "firstName": {
          "type": "string",
          "x-go-name": "FirstName"
        },
        "id": {
          "type": "integer",
          "format": "int64",
          "x-go-name": "ID"
        },
        "lastName": {
          "type": "string",
          "x-go-name": "LastName"
        },
        "password": {
          "type": "string",
          "x-go-name": "Password"
        },
        "phone": {
          "type": "string",
          "x-go-name": "Phone"
        },
        "userStatus": {
          "type": "integer",
          "format": "int64",
          "x-go-name": "UserStatus"
        },
        "username": {
          "type": "string",
          "x-go-name": "Username"
        }
      },
      "x-go-package": "gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"
    }
  },
  "responses": {
    "petAddResponse": {
      "description": "",
      "schema": {
        "$ref": "#/definitions/Pet"
      }
    },
    "petDelResponse": {
      "description": "",
      "schema": {
        "$ref": "#/definitions/Pet"
      }
    },
    "petGetByIDResponse": {
      "description": "",
      "schema": {
        "$ref": "#/definitions/Pet"
      }
    },
    "petGetByStatusResponse": {
      "description": "",
      "schema": {
        "type": "array",
        "items": {
          "$ref": "#/definitions/Pet"
        }
      }
    },
    "petListResponse": {
      "description": "",
      "schema": {
        "type": "array",
        "items": {
          "$ref": "#/definitions/Pet"
        }
      }
    },
    "petUpdIdResponse": {
      "description": "",
      "schema": {
        "$ref": "#/definitions/Pet"
      }
    },
    "petUpdResponse": {
      "description": "",
      "schema": {
        "$ref": "#/definitions/Pet"
      }
    },
    "storeDeleteByIdResponse": {
      "description": "",
      "schema": {
        "type": "array",
        "items": {
          "$ref": "#/definitions/Store"
        }
      }
    },
    "storeGetByIDResponse": {
      "description": "",
      "schema": {
        "$ref": "#/definitions/Store"
      }
    },
    "storeGetByStatusResponse": {
      "description": ""
    },
    "storePlaceAnOrderResponse": {
      "description": "",
      "schema": {
        "$ref": "#/definitions/Store"
      }
    },
    "userCreateResponse": {
      "description": "",
      "schema": {
        "$ref": "#/definitions/User"
      }
    },
    "userCreateWithListResponse": {
      "description": "",
      "schema": {
        "type": "array",
        "items": {
          "$ref": "#/definitions/User"
        }
      }
    },
    "userDeleteByUserNameResponse": {
      "description": "",
      "schema": {
        "type": "array",
        "items": {
          "$ref": "#/definitions/User"
        }
      }
    },
    "userGetByUsernameResponse": {
      "description": "",
      "schema": {
        "$ref": "#/definitions/User"
      }
    },
    "userUpdResponse": {
      "description": "",
      "schema": {
        "$ref": "#/definitions/User"
      }
    }
  },
  "securityDefinitions": {
    "Bearer": {
      "type": "apiKey",
      "name": "Authorization",
      "in": "header"
    }
  }
}