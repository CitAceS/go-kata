package public

import "gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"

// swagger:route DELETE /user/{username} user userDeleteByUserNameRequest
// Delete user.
// responses:
// 200: userDeleteByUserNameResponse

// swagger:parameters userDeleteByUserNameRequest
//
//nolint:all
type userDeleteByUserNameRequest struct {
	// The name that needs to be deleted
	//required:true
	//in:path
	Username string `json:"username"`
}

// swagger:response userDeleteByUserNameResponse
//
//nolint:all
type userDeleteByUserNameResponse struct {
	//in:body
	Body []model.User
}
