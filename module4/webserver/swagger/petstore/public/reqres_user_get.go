package public

import "gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"

// swagger:route GET /user/{username} user userGetByUsernameRequest
// Get user by user name
// responses:
// 200: userGetByUsernameResponse

// swagger:parameters userGetByUsernameRequest
//
//nolint:all
type userGetByUsernameRequest struct {
	//The name that needs to be fetched
	//required:true
	//in:path
	Username string `json:"username"`
}

// swagger:response userGetByUsernameResponse
//
//nolint:all
type userGetByUsernameResponse struct {
	//in:body
	Body model.User
}
