package public

import "gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"

// swagger:route PUT /pet/{id} pet petUpdRequest
// Updates an existing pet.
// responses:
//   200: petUpdResponse

// swagger:parameters petUpdRequest
//
//nolint:all
type petUpdRequest struct {
	// Pet object that needs to be added to the store
	//
	// required:true
	// in:path
	ID string `json:"id"`
	// required:true
	// in:body
	Body model.Pet
}

// swagger:response petUpdResponse
//
//nolint:all
type petUpdResponse struct {
	// in:body
	Body model.Pet
}
