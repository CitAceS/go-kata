package public

import "gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"

// swagger:route GET /pet/{id} pet petGetByIDRequest
// Find pet by ID.
// Returns a single pet
// responses:
//   200: petGetByIDResponse

// swagger:parameters petGetByIDRequest
//
//nolint:all
type petGetByIDRequest struct {
	// ID of pet to return
	//
	// in: path
	ID string `json:"id"`
}

// swagger:response petGetByIDResponse
//
//nolint:all
type petGetByIDResponse struct {
	// in:body
	Body model.Pet
}

// swagger:route GET /pet/findByStatus pet petGetByStatusRequest
// Finds Pets by status.
// Multiple status values can be provided with comma separated strings
// responses:
//  200: petGetByStatusResponse

// swagger:parameters petGetByStatusRequest
//
//nolint:all
type petGetByStatusRequest struct {
	// Status values that need to be considered for filter
	// required: true
	// items.Enum: available, pending, sold
	// in: query
	Status []string `json:"status"`
}

// swagger:response petGetByStatusResponse
//
//nolint:all
type petGetByTagsResponse struct {
	// in:body
	Body []model.Pet
}

// swagger:route GET /pet/listPets pet petListRequest
// List Pets.
// responses:
//  200: petListResponse

// swagger:parameters petListRequest
//
//nolint:all
type petListRequest struct {
	//
	// Shows all pets
}

// swagger:response petListResponse
//
//nolint:all
type petListResponse struct {
	// in:body
	Body []model.Pet
}
