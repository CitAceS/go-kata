package public

import "gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"

// swagger:route PUT /user/{username} user userUpdRequest
// Update user.
// responses:
//   200: userUpdResponse

// swagger:parameters userUpdRequest
//
//nolint:all
type userUpdRequest struct {
	// name that need to be updated
	// required:true
	// in:path
	Username string `json:"username"`
	// required:true
	// in:body
	Body model.User
}

// swagger:response userUpdResponse
//
//nolint:all
type userUpdResponse struct {
	// in:body
	Body model.User
}
