package handlers

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/controllers"
	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/swaggerui"
)

type Handlers struct {
	petControllers   controllers.PetController
	storeControllers controllers.StoreController
	userControllers  controllers.UserController
}

func NewHandlers(petController controllers.PetController,
	storeController controllers.StoreController, userController controllers.UserController) *Handlers {
	return &Handlers{
		petControllers:   petController,
		storeControllers: storeController,
		userControllers:  userController,
	}
}

func (h *Handlers) Route() {
	port := ":8080"

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Post("/user", h.userControllers.UserCreate)
	r.Post("/user/createWithList", h.userControllers.UserCreateWithList)
	r.Get("/user/{username}", h.userControllers.UserGetByUserName)
	r.Put("/user/{username}", h.userControllers.UserUpdByUserName)
	r.Delete("/user/{username}", h.userControllers.UserDelByUserName)
	r.Get("/store/inventory", h.storeControllers.StoreGetByStatus)
	r.Get("/store/order/{storeID}", h.storeControllers.StoreGetByID)
	r.Post("/store/order", h.storeControllers.StoreCreate)
	r.Delete("/store/order/{storeID}", h.storeControllers.StoreDel)
	r.Post("/pet", h.petControllers.PetCreate)
	r.Post("/pet/{petID}", h.petControllers.PetUpdId)
	r.Post("/pet/{petID}/uploadImage", h.petControllers.PetUploadImg)
	r.Delete("/pet/{petID}", h.petControllers.PetDel)
	r.Put("/pet/{petID}", h.petControllers.PetUpdate)
	r.Get("/pet/{petID}", h.petControllers.PetGetByID)
	r.Get("/pet/findByStatus", h.petControllers.PetFindByStatus)
	r.Get("/pet/listPets", h.petControllers.PetList)
	r.Get("/swagger", swaggerui.SwaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/",
			http.FileServer(http.Dir("./module4/webserver/swagger/petstore/public"))).ServeHTTP(w, r)
	})
	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}
	go func() {
		log.Printf("server started on port %s", port)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	log.Println("Server exiting")
}
