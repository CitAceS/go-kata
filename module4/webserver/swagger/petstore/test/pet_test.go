package test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"
	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/repository"
)

var (
	Pet1 = model.Pet{
		ID: 1,
		Category: model.Category{
			ID:   0,
			Name: "test",
		},
		Name:      "gufi",
		PhotoUrls: []string{},
		Tags:      []model.Category{},
		Status:    "pending",
	}

	Pet2 = model.Pet{
		ID: 2,
		Category: model.Category{
			ID:   0,
			Name: "test",
		},
		Name:      "catdog",
		PhotoUrls: []string{"module4/webserver/swagger/petstore/photos/test.jpg"},
		Tags:      []model.Category{},
		Status:    "available",
	}
)

func TestPetStorage_UpdImg(t *testing.T) {
	repo := repository.NewPetStorage()
	repo.Create(Pet1)
	PhotoURL := "test.jpg"
	pet := repo.UpdImg(1, PhotoURL)
	assert.Equal(t, pet.PhotoUrls[0], Pet2.PhotoUrls[0], "photoUpd test")

}

func TestPetStorage_Create(t *testing.T) {
	repo := repository.NewPetStorage()
	pet := repo.Create(Pet1)
	assert.Equal(t, pet, Pet1, "create test")

}

func TestPetStorage_GetByID(t *testing.T) {
	repo := repository.NewPetStorage()
	pet := repo.Create(Pet1)
	pid, err := repo.GetByID(100)
	assert.Error(t, err, "GetByID test")
	assert.Equal(t, model.Pet{}, pid)
	pid, err = repo.GetByID(1)
	assert.NoError(t, err, "GetByID test")
	assert.Equal(t, pet, pid)
}

func TestPetStorage_Update(t *testing.T) {
	repo := repository.NewPetStorage()
	pet := repo.Create(Pet1)
	pet.Status = "sold"
	pid, err := repo.Update(pet)
	assert.NoError(t, err, "Update test")
	assert.Equal(t, pet, pid)
	pet.Status = "pending"
	_, err = repo.Update(Pet2)
	assert.Error(t, err, "Update test")
}

func TestPetStorage_GetList(t *testing.T) {
	repo := repository.NewPetStorage()
	repo.Create(Pet1)
	repo.Create(Pet2)
	assert.Equal(t, 2, len(repo.Data), "GetList test")
}

func TestPetStorage_GetByStatus(t *testing.T) {
	repo := repository.NewPetStorage()
	repo.Create(Pet1)
	repo.Create(Pet2)
	sl := repo.GetByStatus([]string{"available"})
	assert.Equal(t, Pet2, sl[0], "GetByStatus test")
}

func TestPetStorage_Del(t *testing.T) {
	repo := repository.NewPetStorage()
	repo.Create(Pet1)
	repo.Create(Pet2)
	_, err := repo.Del(2)
	assert.NoError(t, err, "test Del")
	assert.Equal(t, 1, len(repo.Data))
	sl, err := repo.Del(123)
	assert.Error(t, err, "test Del")
	assert.Equal(t, []model.Pet{}, sl)
}

func TestPetStorage_UpdById(t *testing.T) {
	repo := repository.NewPetStorage()
	repo.Create(Pet1)
	sl := repo.UpdById(1, "scooby-doo", "pending")
	assert.Equal(t, sl.Name, "scooby-doo")
	assert.Equal(t, sl.Status, "pending")
}
