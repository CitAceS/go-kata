package repository

import (
	"errors"
	"sync"

	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"
)

type UserStorager interface {
	Create(user model.User) (model.User, error)
	CreateWithList(users []model.User) ([]model.User, error)
	GetByUsername(username string) (model.User, error)
	UserUpdByUserName(username string, user model.User) (model.User, error)
	UserDelByUserName(username string) ([]model.User, error)
}

type UserStorage struct {
	Data               []*model.User
	primaryKeyIDx      map[int]*model.User
	primaryUserName    map[string]*model.User
	autoIncrementCount int
	sync.Mutex
}

func NewUserStorage() *UserStorage {
	return &UserStorage{
		Data:               make([]*model.User, 0, 13),
		primaryKeyIDx:      make(map[int]*model.User, 13),
		primaryUserName:    make(map[string]*model.User, 1),
		autoIncrementCount: 1,
	}
}

func (u *UserStorage) Create(user model.User) (model.User, error) {
	u.Lock()
	defer u.Unlock()
	if _, ok := u.primaryUserName[user.Username]; !ok {
		user.ID = u.autoIncrementCount
		u.primaryKeyIDx[user.ID] = &user
		u.primaryUserName[user.Username] = &user
		u.autoIncrementCount++
		u.Data = append(u.Data, &user)
		return user, nil
	}
	return model.User{}, errors.New("this username is reserved, try again")
}

func (u *UserStorage) CreateWithList(users []model.User) ([]model.User, error) {
	u.Lock()
	defer u.Unlock()
	var flag bool
	res := make([]model.User, len(u.Data)+len(users))
	for _, elem := range users {
		if _, ok := u.primaryUserName[elem.Username]; ok {
			flag = true
		}
	}
	switch flag {
	case true:
		return []model.User{}, errors.New("one or more usernames from the list is taken, please try again")
	default:
		for _, user := range users {
			_, _ = u.Create(user)
		}
	}
	for i, user := range u.Data {
		res[i] = *user
	}
	return res, nil
}

func (u *UserStorage) GetByUsername(username string) (model.User, error) {
	u.Lock()
	defer u.Unlock()
	if val, ok := u.primaryUserName[username]; ok {
		return *val, nil
	}
	return model.User{}, errors.New("wrong username, please try again")
}

func (u *UserStorage) UserUpdByUserName(username string, user model.User) (model.User, error) {
	if val, ok := u.primaryUserName[username]; ok {
		for i, elem := range u.Data {
			if val == elem && u.primaryUserName[user.Username] == nil {
				user.ID = val.ID
				u.Data[i] = &user
				u.primaryUserName[user.Username] = &user
				delete(u.primaryUserName, username)
			}
		}
		return user, nil
	}
	return model.User{}, errors.New("wrong username, please try again")
}

func (u *UserStorage) UserDelByUserName(username string) ([]model.User, error) {
	u.Lock()
	defer u.Unlock()
	if len(u.Data) != 0 {
		res := make([]model.User, len(u.Data)-1)
		if v, ok := u.primaryUserName[username]; ok {
			for idx, elem := range u.Data {
				if elem == v {
					a, b := u.Data[:idx], u.Data[idx+1:]
					u.Data = append(a, b...)
					for i, e := range u.Data {
						res[i] = *e
					}
				}
			}
			delete(u.primaryKeyIDx, v.ID)
		} else {
			return []model.User{}, errors.New("wrong username")
		}
		delete(u.primaryUserName, username)
		return res, nil
	}
	return []model.User{}, errors.New("wrong username")

}
