package repository

import (
	"errors"
	"fmt"
	"strings"
	"sync"

	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"
)

type PetStorager interface {
	Create(pet model.Pet) model.Pet
	Update(pet model.Pet) (model.Pet, error)
	GetByID(petID int) (model.Pet, error)
	GetList() []model.Pet
	GetByStatus(petStatus []string) []model.Pet
	Del(id int) ([]model.Pet, error)
	UpdImg(id int, path string) model.Pet
	UpdById(id int, name, status string) model.Pet
}

type PetStorage struct {
	Data               []*model.Pet
	primaryKeyIDx      map[int]*model.Pet
	autoIncrementCount int
	sync.Mutex
}

func NewPetStorage() *PetStorage {
	return &PetStorage{
		Data:               make([]*model.Pet, 0, 13),
		primaryKeyIDx:      make(map[int]*model.Pet, 13),
		autoIncrementCount: 1,
	}
}

func (p *PetStorage) Create(pet model.Pet) model.Pet {
	p.Lock()
	defer p.Unlock()
	pet.ID = p.autoIncrementCount
	p.primaryKeyIDx[pet.ID] = &pet
	p.autoIncrementCount++
	p.Data = append(p.Data, &pet)

	return pet
}

func (p *PetStorage) Update(pet model.Pet) (model.Pet, error) {
	p.Lock()
	defer p.Unlock()
	fmt.Println(pet.ID, p.primaryKeyIDx[pet.ID])
	if _, ok := p.primaryKeyIDx[pet.ID]; ok {
		p.primaryKeyIDx[pet.ID] = &pet
		p.Data[pet.ID-1] = &pet
		return pet, nil
	}
	return model.Pet{}, fmt.Errorf("not found")
}

func (p *PetStorage) GetByID(petID int) (model.Pet, error) {
	p.Lock()
	defer p.Unlock()
	if v, ok := p.primaryKeyIDx[petID]; ok {
		return *v, nil
	}
	return model.Pet{}, fmt.Errorf("not found")
}

func (p *PetStorage) GetByStatus(petStatus []string) []model.Pet {
	p.Lock()
	defer p.Unlock()
	status := strings.Fields(strings.Join(petStatus, ""))
	res := make([]model.Pet, 0, len(p.Data))
	for _, pet := range p.Data {
		if pet.Status == status[0] {
			res = append(res, *pet)
		}
	}
	return res
}

func (p *PetStorage) GetList() []model.Pet {
	p.Lock()
	defer p.Unlock()
	res := make([]model.Pet, len(p.Data))
	for i, elem := range p.Data {
		res[i] = *elem
	}
	return res
}

func (p *PetStorage) Del(id int) ([]model.Pet, error) {
	p.Lock()
	defer p.Unlock()
	if len(p.Data) != 0 {
		res := make([]model.Pet, len(p.Data)-1)
		if v, ok := p.primaryKeyIDx[id]; ok {
			for idx, elem := range p.Data {
				if elem == v {
					a, b := p.Data[:idx], p.Data[idx+1:]
					p.Data = append(a, b...)
					for i, e := range p.Data {
						res[i] = *e
					}
				}
			}

		} else {
			return []model.Pet{}, errors.New("wrong id")
		}
		delete(p.primaryKeyIDx, id)
		return res, nil
	}
	return []model.Pet{}, errors.New("wrong id")

}

func (p *PetStorage) UpdImg(id int, path string) model.Pet {
	p.Lock()
	defer p.Unlock()
	if val, ok := p.primaryKeyIDx[id]; ok {
		s := fmt.Sprintf("module4/webserver/swagger/petstore/photos/%s", path)
		val.PhotoUrls = append(val.PhotoUrls, s)
		return *val
	}
	return model.Pet{}
}

func (p *PetStorage) UpdById(id int, name, status string) model.Pet {
	p.Lock()
	defer p.Unlock()
	if val, ok := p.primaryKeyIDx[id]; ok {
		val.Name = name
		val.Status = status
		return *val
	}
	return model.Pet{}
}
