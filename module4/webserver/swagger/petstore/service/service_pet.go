package service

import (
	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"
	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/repository"
)

type PetService interface {
	PetNew(pet model.Pet) model.Pet
	PetUpdate(pet model.Pet) (model.Pet, error)
	PetGetByID(petID int) (model.Pet, error)
	PetGetByStatus(petStatus []string) []model.Pet
	PetListAll() []model.Pet
	PetDelId(id int) ([]model.Pet, error)
	PetUpdImg(id int, path string) model.Pet
	PetUpdById(id int, name, status string) model.Pet
}

type PetServ struct {
	repo repository.PetStorager
}

func NewPetServ(repository repository.PetStorager) *PetServ {
	return &PetServ{repository}
}

func (p *PetServ) PetNew(pet model.Pet) model.Pet {
	return p.repo.Create(pet)
}

func (p *PetServ) PetUpdate(pet model.Pet) (model.Pet, error) {
	return p.repo.Update(pet)
}

func (p *PetServ) PetGetByID(petID int) (model.Pet, error) {
	return p.repo.GetByID(petID)
}

func (p *PetServ) PetGetByStatus(petStatus []string) []model.Pet {
	return p.repo.GetByStatus(petStatus)
}
func (p *PetServ) PetListAll() []model.Pet {
	return p.repo.GetList()
}

func (p *PetServ) PetDelId(id int) ([]model.Pet, error) {
	return p.repo.Del(id)
}

func (p *PetServ) PetUpdImg(id int, path string) model.Pet {
	return p.repo.UpdImg(id, path)
}

func (p *PetServ) PetUpdById(id int, name, status string) model.Pet {
	return p.repo.UpdById(id, name, status)
}
