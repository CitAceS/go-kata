package service

import (
	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"
	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/repository"
)

type UserService interface {
	UserCreate(user model.User) (model.User, error)
	UserCreateWithList(users []model.User) ([]model.User, error)
	UserGetByUsername(username string) (model.User, error)
	UserUpdByUserName(username string, user model.User) (model.User, error)
	UserDelByUserName(username string) ([]model.User, error)
}

type UserServ struct {
	repo repository.UserStorager
}

func NewUserServ(repository repository.UserStorager) *UserServ {
	return &UserServ{repository}
}

func (u *UserServ) UserCreate(user model.User) (model.User, error) {
	return u.repo.Create(user)
}

func (u *UserServ) UserCreateWithList(users []model.User) ([]model.User, error) {
	return u.repo.CreateWithList(users)
}

func (u *UserServ) UserGetByUsername(username string) (model.User, error) {
	return u.repo.GetByUsername(username)
}

func (u *UserServ) UserUpdByUserName(username string, user model.User) (model.User, error) {
	return u.repo.UserUpdByUserName(username, user)
}
func (u *UserServ) UserDelByUserName(username string) ([]model.User, error) {
	return u.repo.UserDelByUserName(username)
}
