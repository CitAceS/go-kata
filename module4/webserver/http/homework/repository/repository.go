package repository

import (
	"errors"

	"gitlab.com/citaces/go-kata/module4/webserver/http/homework/model"
)

type Repository interface {
	GetUsers() []model.User
	GetUser(id string) (model.User, error)
	CreateUser(user model.User)
}

type UsersRepository struct {
	Users []model.User
}

func NewUsersRepository() *UsersRepository {
	return &UsersRepository{
		Users: []model.User{
			{ID: "1", Username: "bobby", Phone: "880055535"},
			{ID: "2", Username: "gobby", Phone: "123456789"},
			{ID: "3", Username: "tobby", Phone: "987654321"},
		},
	}
}

func (r *UsersRepository) GetUsers() []model.User {
	return r.Users
}

func (r *UsersRepository) GetUser(id string) (model.User, error) {
	for _, user := range r.Users {
		if user.ID == id {
			return user, nil
		}
	}
	return model.User{}, errors.New("not found")
}

func (r *UsersRepository) CreateUser(user model.User) {
	r.Users = append(r.Users, user)
}
